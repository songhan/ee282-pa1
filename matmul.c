#include <x86intrin.h>
#include <stdio.h>

// This is the matrix multiply kernel you are to replace.
// Note that you are to implement C = C + AB, not C = AB!

void matmul(int N, const double* A, const double* B, double* C) {
	if(1 && N==2) // SSE trial
	{
		__m128d A_line1, A_line2;
		__m128d A_line1r, A_line2r;
		__m128d B_line1, B_line2;
		__m128d C_line1, C_line2;
		__m128d temp1, temp2, temp3, temp4;

		A_line1 = _mm_load_pd(A);
		A_line1r = _mm_loadr_pd(A);
		A_line2 = _mm_load_pd(A+2);
		A_line2r = _mm_loadr_pd(A+2);

		B_line1 = _mm_loadl_pd(B_line1, B);
		B_line1 = _mm_loadh_pd(B_line1, B+3);
		B_line2 = _mm_loadl_pd(B_line2, B+2);
		B_line2 = _mm_loadh_pd(B_line2, B+1);

		C_line1 = _mm_load_pd(C);
		C_line2 = _mm_load_pd(C+2);

		temp1 = _mm_mul_pd(A_line1, B_line1);
		temp2 = _mm_mul_pd(A_line1r, B_line2);
		temp3 = _mm_mul_pd(A_line2, B_line1);
		temp4 = _mm_mul_pd(A_line2r, B_line2);

		C_line1 = _mm_add_pd(C_line1, temp1);
		C_line1 = _mm_add_pd(C_line1, temp2);
		C_line2 = _mm_add_pd(C_line2, temp3);
		C_line2 = _mm_add_pd(C_line2, temp4);

		_mm_store_pd(C, C_line1);
		_mm_store_pd(C+2, C_line2);

		return;
	}
  int i, j, k;
  int ib_max, jb_max, kb_max;
  int ib, jb, kb;
  int B1=1;
  
 // printf ("%p, %p, %p\n", A+1, B+1, C+1);
  if (N>16)
     B1=16;
  for (i = 0; i < N; i+=B1)  {
    for (j = 0; j < N; j+=B1)  {
      for (k = 0; k < N; k+=B1) {
        ib_max=(i+B1<N)?(i+B1):N;
        for(ib=i; ib<ib_max; ib++) {
           jb_max=(j+B1<N)?(j+B1):N;
           for(jb=j;jb<jb_max;jb++) {
              kb_max=(k+B1<N)?(k+B1):N;
              for(kb=k; kb<kb_max; kb++){
                 C[ib*N+jb]+=A[ib*N+kb]*B[kb*N+jb];
              }
           }         
        } 
      }
    }
  }
}
